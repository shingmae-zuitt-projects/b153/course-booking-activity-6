// let params = new URLSearchParams(window.location.search)
//MY ACTIVITY

/*
let courseId = params.get("courseId")
let deleteCourse= document.querySelector('#disableCourse')

fetch(`http://localhost:4000/courses/${courseId}`)
.then(res => res.json())
.then(data => {

    fetch(`http://localhost:4000/courses/${courseId}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                alert("Course successfully Disabled.")
                window.location.replace("./deleteCourse.html")
            }else{
                alert("Something went wrong. Please try again")
            }
        })
    
})

*/

let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")

let token = localStorage.getItem("token")

fetch(`http://localhost:4000/courses/${courseId}`, {
    method: "DELETE",
    headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
}
})
.then(res => res.json())
.then(data => {
    if(data){
        window.location.replace("./courses.html")
    }else{
        alert("Something went wrong. Please try again.")
    }
})